document.addEventListener('DOMContentLoaded', function() {
    EventListeners(); //funcion para accion de click en menu hamburguesa
    darkMode();
});

function darkMode() {

    const prefiereDarkMode = window.matchMedia('(prefers-color-scheme: dark)');
    if (prefiereDarkMode.matches) {
        document.body.classList.add('dark-mode');
    } else {
        document.body.classList.remove('dark-mode');
    }

    prefiereDarkMode.addEventListener('change', function() { 
        if (prefiereDarkMode.matches) {
            document.body.classList.add('dark-mode');
        } else {
            document.body.classList.remove('dark-mode');
        }
    });

    const botonDarkMode = document.querySelector('.dark-mode-boton');
    botonDarkMode.addEventListener('click', function(){
        document.body.classList.toggle('dark-mode'); //agrega o elimina esta clase al body
    });
}

function EventListeners() {
    const mobilemenu = document.querySelector('.mobile-menu');
    mobilemenu.addEventListener('click', navegacionResponsive);
}

function navegacionResponsive() { //canbiamos el display none para mostrar el menu
    const navegacion = document.querySelector('.navegacion');

    //navegacion.classList.toggle('mostrar'); // Esta linea de codigo hace exactamente lo mismo que el if, agregar o eliminar la clase 
    
    if (navegacion.classList.contains('mostrar')) {
        navegacion.classList.remove('mostrar');
    } else {
        navegacion.classList.add('mostrar');
    }
}