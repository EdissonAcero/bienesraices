<?php 
	require 'includes/funciones.php';
	incluirTemplate('header');
?>
    
    <main class="contenedor seccion">
    	<h1>Conoce Sobre Nosotros</h1>
		<div class="contenido-nosotros">
			<div class="imagen">
				<picture>
					<source srcset="build/img/nosotros.webp" type="image/webp">
					<source srcset="build/img/nosotros.jpg" type="image/jpeg">
					<img loading="lazy" src="build/img/nosotros.jpg" alt="sobre nosotros">
				</picture>
			</div>
			<div class="texto-nosotros">
				<blockquote> 25 AÑOS DE EXPERIENCIA </blockquote>
				<p> Praesent placerat eleifend metus sed commodo. Nam iaculis nisl consectetur lorem luctus gravida. 
					Suspendisse aliquam malesuada diam et varius. Mauris et vehicula ex. Proin id pharetra nulla, ac porta orci. 
					Sed nec tellus sed leo volutpat mattis. Nam gravida cursus augue non sagittis. Pellentesque dapibus
					auctor lorem, id tincidunt sem lobortis at. Nunc porttitor eu velit vitae volutpat. Donec tincidunt 
					tellus at diam suscipit, eu tempor arcu lacinia. Quisque aliquam ullamcorper dui, ut bibendum sem suscipit ut. 
					Duis porttitor erat eget convallis dapibus. Vivamus a mi ac erat porttitor congue. Cras hendrerit dictum nulla 
					vel consequat. Ut scelerisque, lorem ut tincidunt convallis, sapien erat tristique nunc, in mollis nibh lacus 
					eu mauris. Mauris fermentum maximus nunc in pellentesque. Pellentesque volutpat eleifend nibh nec luctus.
				</p>
				<p>Phasellus vitae odio vitae est pellentesque euismod in nec erat. Donec at massa et enim ultricies commodo. 
					Aenean condimentum porttitor suscipit. Vestibulum eget massa ac turpis feugiat egestas. Nulla facilisi. 
					Mauris quis aliquam dolor. Sed a felis in mauris consequat tempus ac ut ex.  
				</p>
			</div>
		</div>
    </main>

	<section class="contenedor seccion">
    	<h1>Mas Sobre Nosotros</h1>
		
		<div class="iconos-nosotros">
			<div class="icono">
				<img src="build/img/icono1.svg" alt="Icono seguridad" loading="lazy">
				<h3>Seguridad</h3>
				<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias aliquid nostrum quaerat voluptatibus 
					tenetur sequi temporibus vel, doloremque consectetur facere, animi quo, quos obcaecati enim ea 
					aspernatur blanditiis minus hic.</p>
			</div>

			<div class="icono">
				<img src="build/img/icono2.svg" alt="Icono precio" loading="lazy">
				<h3>Precio</h3>
				<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias aliquid nostrum quaerat voluptatibus 
					tenetur sequi temporibus vel, doloremque consectetur facere, animi quo, quos obcaecati enim ea 
					aspernatur blanditiis minus hic.</p>
			</div>

			<div class="icono">
				<img src="build/img/icono3.svg" alt="Icono tiempo" loading="lazy">
				<h3>A Tiempo</h3>
				<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias aliquid nostrum quaerat voluptatibus 
					tenetur sequi temporibus vel, doloremque consectetur facere, animi quo, quos obcaecati enim ea 
					aspernatur blanditiis minus hic.</p>
			</div>
		</div>
	</section>
    
<?php 
	incluirTemplate('footer');
?>