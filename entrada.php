<?php 
	require 'includes/funciones.php';
	incluirTemplate('header');
?>
    
    <main class="contenedor seccion contenido-centrado">
    	<h1>Guia para la decoracion de tu hogar</h1>
		
		<picture>
			<source srcset="build/img/destacada2.webp" type="image/webp">
			<source srcset="build/img/destacada2.jpg" type="image/jpeg">
			<img loading="lazy"src="build/img/destacada2.jpg" alt="imagen de la propiedad">
		</picture>
		
		<p class="informacion-meta">Escrito el: <span>18/06/2022</span> por: <span>Admin</span></p>

		<div class="resumen-propiedad">
			<p>
				Sed lacinia semper libero eget dignissim. Proin feugiat vel ligula elementum ullamcorper. 
				Proin orci lorem, accumsan at scelerisque pulvinar, accumsan non libero. Sed commodo aliquam tellus ut euismod. 
				Integer iaculis purus in suscipit porttitor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
				Morbi arcu urna, interdum ac nisi eu, pulvinar luctus magna. Sed vitae erat eu nisi blandit aliquam sit amet nec tellus. 
				Etiam maximus, nibh sit amet sodales fermentum, felis dolor vulputate justo, at pretium lacus nulla nec felis. 
				Donec ac imperdiet urna. Sed ut rhoncus sem. Etiam et dictum orci. Nunc porttitor ac justo eget posuere.
			</p>
			<p>
				Sed elit dui, ornare quis aliquam eu, semper a purus. Maecenas volutpat egestas urna ut laoreet. 
				Nam efficitur venenatis tortor sit amet pharetra. Integer sit amet sollicitudin urna. Quisque tincidunt blandit semper. 
				Vestibulum ultrices vehicula orci ut malesuada. Nullam laoreet, felis vitae sollicitudin tempus, eros turpis dictum nibh, 
				sit amet efficitur quam nibh ac enim. Donec pharetra maximus augue id placerat. 
				Aenean sit amet purus interdum, lobortis sapien vel, mattis est. Quisque ultricies nulla non nisi dignissim sagittis. 
				Maecenas ac luctus libero. Quisque semper feugiat mi, vitae condimentum dolor vestibulum id. 
				Quisque tempor ipsum ut est gravida, sit amet mollis leo accumsan. Quisque quis sem urna. Phasellus et viverra lectus, 
				eget interdum nulla. Etiam scelerisque cursus leo, a tincidunt felis faucibus ut.
			</p>
			
    </main>
    
<?php 
	incluirTemplate('footer');
?>