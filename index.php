<?php 
	require 'includes/funciones.php';
	incluirTemplate('header', $inicio = true);
?>
    
    <main class="contenedor seccion">
    	<h1>Mas Sobre Nosotros</h1>
		
		<div class="iconos-nosotros">
			<div class="icono">
				<img src="/build/img/icono1.svg" alt="Icono seguridad" loading="lazy">
				<h3>Seguridad</h3>
				<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias aliquid nostrum quaerat voluptatibus 
					tenetur sequi temporibus vel, doloremque consectetur facere, animi quo, quos obcaecati enim ea 
					aspernatur blanditiis minus hic.</p>
			</div>

			<div class="icono">
				<img src="/build/img/icono2.svg" alt="Icono precio" loading="lazy">
				<h3>Precio</h3>
				<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias aliquid nostrum quaerat voluptatibus 
					tenetur sequi temporibus vel, doloremque consectetur facere, animi quo, quos obcaecati enim ea 
					aspernatur blanditiis minus hic.</p>
			</div>

			<div class="icono">
				<img src="/build/img/icono3.svg" alt="Icono tiempo" loading="lazy">
				<h3>A Tiempo</h3>
				<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias aliquid nostrum quaerat voluptatibus 
					tenetur sequi temporibus vel, doloremque consectetur facere, animi quo, quos obcaecati enim ea 
					aspernatur blanditiis minus hic.</p>
			</div>
		</div>
    </main>

	<section class="contenedor seccion">
		
		<h2>Casas y Departamentos en Venta</h2>
		
		<div class="contenedor-anuncios">
			<div class="anuncio">
				
				<picture >
					<source srcset="/build/img/anuncio1.webp" type="image/webp">
					<source srcset="/build/img/anuncio1.jpeg" type="image/jpeg">
					<img loading="lazy" src="build/img/anuncio1.jpg" alt="anuncio">
				</picture>

				<div class="contenido-anuncio">
					<h3>Casa de Lujo en el Lago</h3>
					
					<p>Casa en el lago con exelente vista, con acabados de lujo a un exelente precio</p>
					
					<p class="precio">$3.000.000</p>
					
					<ul class="iconos-caracteristicas">
						<li>
							<img class="icono" loading="lazy" src="/build/img/icono_wc.svg" alt="icono wc">
							<p>3</p>
						</li>
						<li>
							<img class="icono" loading="lazy" src="/build/img/icono_estacionamiento.svg" alt="icono estacionamiento">
							<p>3</p>
						</li>
						<li>
							<img class="icono" loading="lazy" src="/build/img/icono_dormitorio.svg" alt="icono habitaciones">
							<p>4</p>
						</li>
					</ul>

					<a href="anuncios.php" class="boton-amarillo-block"> ver propiedad </a>
				
				</div><!-- .contenido-anuncio -->
			</div><!-- .anuncio -->

			<div class="anuncio">
				
				<picture >
					<source srcset="/build/img/anuncio2.webp" type="image/webp">
					<source srcset="/build/img/anuncio2.jpeg" type="image/jpeg">
					<img loading="lazy" src="/build/img/anuncio1.jpg" alt="anuncio">
				</picture>

				<div class="contenido-anuncio">
					<h3>Casa terminados de lujo</h3>
					
					<p>Casa con diseño moderno y tecnologia inteligente, diseño amueblado</p>
					
					<p class="precio">$8.000.000</p>
					
					<ul class="iconos-caracteristicas">
						<li>
							<img class="icono" loading="lazy" src="build/img/icono_wc.svg" alt="icono wc">
							<p>3</p>
						</li>
						<li>
							<img class="icono" loading="lazy" src="build/img/icono_estacionamiento.svg" alt="icono estacionamiento">
							<p>3</p>
						</li>
						<li>
							<img class="icono" loading="lazy" src="build/img/icono_dormitorio.svg" alt="icono habitaciones">
							<p>4</p>
						</li>
					</ul>

					<a href="anuncios.php" class="boton-amarillo-block"> ver propiedad </a>
				
				</div><!-- .contenido-anuncio -->
			</div><!-- .anuncio -->

			<div class="anuncio">
				
				<picture >
					<source srcset="build/img/anuncio3.webp" type="image/webp">
					<source srcset="build/img/anuncio3.jpeg" type="image/jpeg">
					<img loading="lazy" src="build/img/anuncio1.jpg" alt="anuncio">
				</picture>

				<div class="contenido-anuncio">
					<h3>Casa con picina</h3>
					
					<p>Casa con piscina y acabados de lujo en la ciudad, exelente oportunidad</p>
					
					<p class="precio">$5.000.000</p>
					
					<ul class="iconos-caracteristicas">
						<li>
							<img class="icono" loading="lazy" src="build/img/icono_wc.svg" alt="icono wc">
							<p>3</p>
						</li>
						<li>
							<img class="icono" loading="lazy" src="build/img/icono_estacionamiento.svg" alt="icono estacionamiento">
							<p>3</p>
						</li>
						<li>
							<img class="icono" loading="lazy" src="build/img/icono_dormitorio.svg" alt="icono habitaciones">
							<p>4</p>
						</li>
					</ul>

					<a href="anuncios.php" class="boton-amarillo-block"> ver propiedad </a>
				
				</div><!-- .contenido-anuncio -->
			</div><!-- .anuncio -->

		</div><!-- .contenedor-anuncio -->

		<div class="ver-todas">
			<a href="anuncios.php" class="boton-verde">Ver Todas</a>
		</div>
	</section>

    <section class="imagen-contacto">
		<h2>Encuentra la casa de tus sueños</h2>
		<p>Llena el formulario de contacto y un asesor se comunicara con tigo</p>
		<a href="contacto.php" class="boton-amarillo">Contactanos</a>
	</section>

	<div class="contenedor seccion seccion-inferior">
		<section class="blog">
			<h3>Nuestro Blog</h3> 
			<article class="entrada-blog">
				<div class="imagen">
					<picture>
						<source srcset="build/img/blog1.webp" type="image/webp">
						<source srcset="build/img/blog1.jpg" type="image/jpeg">
						<img loading="lazy" src="build/img/blog1.jpg" alt="Texto entrada blog">
					</picture>					
				</div>

				<div class="texto-entrada">
					<a href="entrada.php">
						<h4>Terraza en el techo de tu casa</h4>
						<p class="informacion-meta">Escrito el: <span>18/06/2022</span> por: <span>Admin</span></p>
						<p>Consejos para construir una terraza en el techo de tu casa con los mejores materiales
							aorrando dinero.
						</p>
					</a>
				</div>

			</article>

			<article class="entrada-blog">
				<div class="imagen">
					<picture>
						<source srcset="build/img/blog2.webp" type="image/webp">
						<source srcset="build/img/blog2.jpg" type="image/jpeg">
						<img loading="lazy" src="build/img/blog2.jpg" alt="Texto entrada blog">
					</picture>					
				</div>

				<div class="texto-entrada">
					<a href="entrada.php">
						<h4>Guia para la decoracion de tu hogar</h4>
						<p class="informacion-meta">Escrito el: <span>18/06/2022</span> por: <span>Admin</span></p>
						<p>Maximisa el espacio en tu hogar con esta guia, 
							aprende a combinar los colores y muebles para darle
							vida a tu espacio.
						</p>
					</a>
				</div>
				
			</article>
		</section>

		<section class="testimoniales">
			<h3>Testimoniales</h3>

			<div class="testimonial">
				<blockquote>
					El comportamiento del personal es exelente,
					una atencion al detalle exelente, y lo que 
					me ofecen supera las expectativas.
				</blockquote>
				<p>Edisson Acero</p>
			</div>
		</section>

	</div>

<?php 
	incluirTemplate('footer');
?>
